#include "test.h"
#include <sys/ipc.h>
#include <sys/wait.h>
#include <errno.h>

//#include <drivers/char/testdev.h>

unsigned char wr_buf[50]="hello test_dev";
unsigned char re_buf[50]={0};
int drivers_test(int argc, char *argv[])
{
    int fd = open("/dev/testdev", O_RDONLY);
    if (fd < 0) {
        printf("open failed!\n");
        return -1;
    }

    int wb = write(fd, wr_buf, strlen(wr_buf));
    printf("write: bytes %d\n", wb);

    int rb = read(fd, re_buf, strlen(wr_buf));
    printf("read: bytes %d\n", rb);
    printf(" %s\n", re_buf);
    close(fd);
}