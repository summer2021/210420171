#include "test.h"
#include <sys/ipc.h>
#include <sys/wait.h>
#include <errno.h>


static unsigned char buf[0x1000*20];
int ac97_test(int argc, char *argv[])
{
    int fd = open("/dev/ac97", O_RDONLY);
    if (fd < 0) {
        printf("open failed!\n");
        return -1;
    }

    int  fd2=open("/usr/data/binyu48k16bit.pcm",O_RDONLY);
    if (fd2 < 0) {
        printf("pcm file open failed!\n");
        return -1;
    }
    int rb = read(fd2, buf,sizeof(buf));
    printf("read pcm file : bytes %d\n", rb);

    int wb = write(fd, buf, rb);
    printf("write ac97 driver: bytes %d\n", wb);

    close(fd);
    close(fd2);
}