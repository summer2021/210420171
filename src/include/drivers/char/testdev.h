#ifndef _TESTDEV_H
#define _TESTDEV_H
#include <xbook/driver.h>

iostatus_t testdev_write(device_object_t *device, io_request_t *ioreq);

iostatus_t testdev_read(device_object_t *device, io_request_t *ioreq);


#endif   /* _TESTDEV_H */