#ifndef _AC97_H
#define _AC97_H
#include <xbook/driver.h>

iostatus_t ac97_write(device_object_t *device, io_request_t *ioreq);

iostatus_t ac97_read(device_object_t *device, io_request_t *ioreq);


#endif   /* _AC97_H */